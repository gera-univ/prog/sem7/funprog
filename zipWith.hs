xZipWith :: (a -> a -> a) -> [a] -> [a] -> [a]

xZipWith f [] _ = []
xZipWith f _ [] = []
xZipWith f a b = f (head a) (head b) : (xZipWith f (drop 1 a) (drop 1 b))
