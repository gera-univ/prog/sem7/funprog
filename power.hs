power :: (Integral n, Num x) => n -> x -> x

power 0 x = 1
power 1 x = x
power 2 x = x*x

power n x
  | n `mod` 2 == 1 = x * (power (pred n) x)
  | otherwise = (power (div n 2) x) * (power 2 x)
