clone :: Int -> [a] -> [a]

clone n a = concat [replicate n x | x <- a]
