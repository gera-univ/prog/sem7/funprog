evalPolynomial :: (Num a) => a -> [a] -> a

evalPolynomial x a = sum (zipWith (*) [x^i | i <- reverse [0 .. length a - 1]] a)
