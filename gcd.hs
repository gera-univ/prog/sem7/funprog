gcd' :: Int -> Int -> Int

gcd' a b
  | b == a = a
  | a > b = gcd' (a - b) b
  | otherwise = gcd' a (b - a)
